﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {
    public paddle paddle;
    private Vector3 paddleToBallVector;
    
	// Use this for initialization
	void Start () {
        paddleToBallVector = this.transform.position - paddle.transform.position;
       // print("this is it"+paddleToBallVector.y);
	}
	
	// Update is called once per frame
	void Update () {

        this.transform.position = paddle.transform.position + paddleToBallVector;
        if(Input.GetMouseButton(0))
        print("Mouse button clicked");
		
	}
}
